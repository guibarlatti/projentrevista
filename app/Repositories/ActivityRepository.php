<?php

namespace App\Repositories;
 
use App\Activity;
 
class ActivityRepository
{
	private $model;
 
	public function __construct(Activity $model)
	{
		$this->model = $model;
	}
 
	public function findAll()
	{
		return $this->model->with('status')->get();
	}

	public function find($id){
		return $this->model->find($id);
	}

	public function save($request){
		$activity = $this->model;
		$activity->fill($request->all());
		$response = $activity->save();
		return array(
			"status" => ($response) ? 'success': 'error',
			"object" => $activity
		);
	}

	public function delete($id){
		$activity = $this->model;
		$response = $activity->find($id)->delete();
		return array(
			"status" => ($response) ? 'success': 'error',
			"object" => $activity
		);
	}

	public function getById($id){
		$activity = $this->model;
		$response = $activity->where('id', $id)->with('status')->first();
		return $response;
	}

	public function update($request, $id){
		$activity = $this->model;
		$response = $activity->find($id)->update($request->input());

		return array(
			"status" => ($response) ? 'success': 'error',
			"object" => $activity
		);
	}

	public function filterList($situation, $status){
		$busca = $this->model->with('status');
		if($situation != '0'){
			$busca->where('situation', $situation);
		}
		if($status != '0'){
			$busca->where('status_id', $status)->get();
		}

		return $busca->get();
				
	}

}