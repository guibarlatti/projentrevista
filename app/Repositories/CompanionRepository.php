<?php

namespace App\Repositories;
 
use App\Status;
 
class StatusRepository
{
	private $model;
 
	public function __construct(Status $model)
	{
		$this->model = $model;
	}
 
	public function findAll()
	{
		return $this->model->all();
	}

	public function save($request){
		$status = $this->model;
		$status->fill($request->all());
		$response = $status->save();
		return array(
			"status" => ($response) ? 'success': 'error',
			"object" => $status
		);
	}
}