<?php

namespace App\Http\Controllers;

use App\Repositories\ActivityRepository as Repository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ActivityController extends Controller
{

    public function __construct(Repository $repository){
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = $this->repository->findAll();
        $status = DB::table('status')->get();
        return view('entrevista.listagem', compact('activities', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = DB::table('status')->get();
        return view('entrevista.cadastro', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->repository->save($request);

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $activity = $this->repository->getById($id);
        $status = DB::table('status')->get();
        return view('entrevista.cadastro', compact('status', 'activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->repository->update($request, $id);
        
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->repository->delete($id);
        
        return redirect()->route('activity.index');
    }

    public function listFilter($situation, $status){
        $activities = $this->repository->filterList($situation, $status);
        $status = DB::table('status')->get();
        return view('entrevista.listagem', compact('activities', 'status'));
    }
}
