<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::Routes();

Route::get('activity/{id}/delete', ['uses' => 'ActivityController@destroy', 'as' => 'activity.delete']);

Route::resource('activity', 'ActivityController');
Route::resource('status', 'StatusController');

Route::get('activity/{situation?}/{status?}', 'ActivityController@listFilter');
