@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home Page</div>

                <div class="panel-body">
                    <li><a href="{{ url('/activity') }}">Cadastro de Atividades</a></li>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
