@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listagem de Atividades</div>

                <div class="panel-body">
                    <label for="name" class="col-md-2 control-label">Filtros: </label>
                    <div class="col-md-3">
                        <select class="form-control" name="situation" id="situation">
                            <option value="" disabled selected>Situação</option>
                            <option value="Ativo" {{ (isset( $activity )) ? (($activity->situation == 'Ativo') ? 'selected' : '')  : ''}}>Ativo</option>
                            <option value="Inativo" {{ (isset( $activity )) ? (($activity->situation == 'Inativo') ? 'selected' : '')  : ''}}>Inativo</option>
                        </select>          
                    </div>
                    <div class="col-md-4">  
                        <select class="form-control" name="status" id="status">
                            <option value="" disabled selected>Status</option>
                            @foreach($status as $stat)
                                <option value="{{$stat->id}}" {{ (isset( $activity )) ? (($activity->status_id == $stat->id) ? 'selected' : '')  : ''}}>{{$stat->name}}</option>
                            @endforeach
                        </select>          
                    </div>
                    <div class="col-md-2">
                        <a href="" id="filtrar" class="btn btn-primary" >Filtrar </a>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Nome</th>
                            <th>Situação</th>
                            <th>Status</th>
                            <th>Descrição</th>
                            <th>Dat.Inicio</th>
                            <th>Dat.Termino</th>
                            <th>Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($activities as $activity)
                            <tr class="{{ ($activity->status->name == 'Concluído') ? 'concluidoFundo' : ''}}">
                            <td>{{ $activity->name }}</td>
                            <td>{{ $activity->situation }}</td>
                            <td>{{ $activity->status->name }}</td>
                            <td>{{ $activity->description }}</td>
                            <td>{{ $activity->startDate }}</td>
                            <td>{{ $activity->finishDate }}</td>
                            <td>
                                <a href="{{ route('activity.edit', $activity->id) }}" class="btn btn-info {{ ($activity->status->name == 'Concluído') ? 'invisible' : ''}}" ><i class="fa fa-pencil-square-o"></i> </a>
                                {{--  <a href="{{ route('activity.delete', $activity->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>  --}}
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <a href="{{ route('activity.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Cadastrar </a>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('#filtrar').on('click', function(e){
        e.preventDefault();
        var situation = $('#situation').val();
        var status = $('#status').val();
        if($('#situation').val() == null){
           situation = '0';
        }
        if($('#status').val() == null){
            status = '0';
        }

        window.location = '/activity/'+situation+'/'+status;
        
        
    });
    
</script>
@endsection
