@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Usuario</div>
                <div class="panel-body">
                    <form class="form-horizontal col-md-12 form" id="form" name="form" data-idActivity="{{ isset($activity) ? $activity->id : '' }}">
                        {{--  {{ csrf_field() }}  --}}

                        <div class="form-group col-md-12">
                            <div class="form-group col-md-12">
                                <label for="name" class="col-md-2 control-label">Nome</label>

                                <div class="col-md-8">
                                    <input id="name" value="{{ (isset($activity)) ? $activity->name : '' }}" type="text" class="form-control" name="name" autofocus>
                                        <span class="help-block">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="situation" class="col-md-2 control-label">Situação</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="situation" id="situation">
                                        <option value="Ativo" {{ (isset( $activity )) ? (($activity->situation == 'Ativo') ? 'selected' : '')  : ''}}>Ativo</option>
                                        <option value="Inativo" {{ (isset( $activity )) ? (($activity->situation == 'Inativo') ? 'selected' : '')  : ''}}>Inativo</option>
                                    </select>          
                                </div>
                            </div>
                            <div class="form-group col-md-12" >
                                <label for="status_id" class="col-md-2 control-label">Status</label>

                                    <div class="col-md-8">
                                        <select class="form-control" name="status_id" id="status_id">
                                            @foreach($status as $stat)
                                                <option value="{{$stat->id}}" {{ (isset( $activity )) ? (($activity->status_id == $stat->id) ? 'selected' : '')  : ''}}>{{$stat->name}}</option>
                                            @endforeach
                                        </select>          
                                    </div>
                            </div>

                            <div class="form-group col-md-12" >
                                <label for="description" class="col-md-2 control-label">Descrição</label>

                                <div class="col-md-8">
                                    <textarea id="description" type="text" class="form-control" name="description" autofocus>{{(isset($activity)) ? $activity->description : '' }}</textarea>
                                        <span class="help-block">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="startDate" class="col-md-2 control-label">Dat.Inicio</label>

                                <div class="col-md-8">
                                    <input id="startDate" type="text" value="{{(isset($activity)) ? $activity->startDate : '' }}" class="form-control datepicker" name="startDate">
                                        <span class="help-block">   
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="finishDate" class="col-md-2 control-label">Dat.Termino</label>

                                <div class="col-md-8">
                                    <input id="finishDate" type="text" value="{{(isset($activity)) ? $activity->finishDate : '' }}" class="form-control datepicker" name="finishDate">
                                        <span class="help-block">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                            <a class="btn btn-danger" href="{{ url('/activity') }}">Cancelar</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {

        $(document).ready(function(){
            $("#startDate").datepicker({
                minDate: 0,
                dateFormat: "yy-mm-dd",
                onClose : function(){
                    $("#finishDate").datepicker("option", "minDate", $("#startDate").datepicker('getDate'))
                }
            });

            $("#finishDate").datepicker({
                minDate: 0,
                dateFormat: "yy-mm-dd",
                onClose : function(){
                    $("#startDate").datepicker("option", "maxDate", $("#finishDate").datepicker('getDate'))
                }
            });

            if($('#status_id').children("option").filter(":selected").text() != 'Concluído'){
                $( "#finishDate" ).rules( "add", {
                    required: true
                });
            }else{
                $( "#finishDate" ).rules( "remove", 'required');
            }

        });

        $('#status_id').on('change', function(){
            if($('#status_id').children("option").filter(":selected").text() != 'Concluído'){
                $( "#finishDate" ).rules( "add", {
                    required: true
                });
            }else{
                $( "#finishDate" ).rules( "remove", 'required');
            }
        }),

        $('#form').on('submit', function (e) {
            e.preventDefault();
            if (!$(this).valid()) return false;

            console.log($('#form').attr('data-idActivity'));

            if($('#form').attr('data-idActivity') != ''){
                $.ajax({
                    type: 'put',
                    url: '/activity/'+$('#form').attr('data-idActivity'),
                    data: $("#form").serialize(),
                    success: function (resp) {
                        alert('Registro alterado !!!');
                        window.location = "/activity";
                    }
                });
            }else{
                $.ajax({
                    type: 'post',
                    url: '/activity/',
                    data: $("#form").serialize(),
                    success: function (resp) {
                        alert('Cadastro efetuado !!!');
                        window.location = "/activity";
                    }
                });
            }
            
        });

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 255,
                },
                situation: {
                    required: true,
                },
                status_id: {
                    required: true,
                },
                description: {
                    required: true,
                    maxlength: 600,
                },
                startDate: {
                    required: true,
                },
            },
            messages: {
                name: "O campo nome é obrigatorio",
                description: "O campo descrição é obrigatorio",
                startDate: "O campo data de inicio é obrigatorio",
                situation: "O campo data situação é obrigatorio",
                finishDate: "O campo data termino é obrigatorio",
            },
        });
    });

</script>
@endsection
