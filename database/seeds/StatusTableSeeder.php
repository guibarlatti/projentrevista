<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            'name' => "Pendente",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('status')->insert([
            'name' => "Em Desenvolvimento",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('status')->insert([
            'name' => "Em Teste",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('status')->insert([
            'name' => "Concluído",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
